# ROS Test Communication Tutorials (PYTHON)

Personal testing of ros communication between ubuntu16.04 and raspbian buster
Following: http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29

## Instructions on using


### Create catkin workspace

    mkdir -p ros-test-communication/src
    cd ros-test-communication/
    catkin_make

### Clone repository and build

    cd src/
    git clone https://gitlab.com/ijnek/ros-test-communication-tutorials.git
    cd ../
    catkin_make

### Run roscore

    roscore

### Run Talker, in new terminal

    source devel/setup.bash
    rosrun ros-test-communication-tutorials talker.py

### Run Listener, in new terminal

    source devel/setup.bash
    rosrun ros-test-communication-tutorials listener.py
